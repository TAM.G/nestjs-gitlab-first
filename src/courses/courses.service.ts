import { Injectable, HttpException } from '@nestjs/common';
import {COURSES} from './courses.mock';
import { resolve } from 'path';
import { promisify } from 'util';
import { promises } from 'dns';

@Injectable()
export class CoursesService {
    courses = COURSES;

    getCourses() : Promise<any> {
        return new Promise (resolve => {
            resolve(this.courses);
        });
    }

    getCourse(courseId) : Promise<any>{
        let id = Number(courseId);
        return new Promise(resolve => {
            const course = this.courses.find(course => course.id === id);
            if(!course)
            throw new HttpException('No Course Found', 404);
            resolve(course);
        });
    }

    addCourse(course) : Promise<any>{
        return new Promise(resolve => {
            this.courses.push(course);
            this.courses.fill(course);
            resolve(this.courses);
        });
    }

    deleteCourse(courseId) : Promise<any>{
        let id = Number(courseId);
        return new Promise (resolve => {
            let index = this.courses.findIndex(course => course.id === id);
            console.log("index" + index);
            if(index === -1)
            throw new HttpException('error', 404);
            this.courses.slice(index);
            resolve(this.courses);
        });
    }
}
